<?php
/**
 * @file
 * Admin page callback for Custom Language Block.
 */

/**
 * Page callback for Custom Language Block settings.
 *
 * Allows users to:
 * - toggle visibility of current active language link.
 * - choose which language name will be displayed:
 * native, english or language code.
 * - @todo add separator between links.
 *
 */
function custom_language_block_form($form, &$form_state) {
  $form['custom_language_block_exclude_active'] = array(
    '#type' => 'checkbox',
    '#title' => t('Exclude active language'),
    '#description' => t('If selected active language link will not be displayed.'),
    '#default_value' => variable_get('custom_language_block_exclude_active', 0),
  );
  $form['custom_language_block_choose_names'] = array(
    '#type' => 'radios',
    '#title' => t('Language name'),
    '#description' => t('Choose which language name will be displayed.'),
    '#options' => array(
    	'native' => t('Use native language'),
    	'english' => t('Use english language name'),
    	'code' => t('Use language code'),
    ),
		'#required' => TRUE,
		'#default_value' =>  variable_get('custom_language_block_choose_names', 'native'),
  );
  $form['custom_language_block_use_short_names'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use short names'),
    '#description' => t('If selected the first two characters of the chosen language names will be used.'),
    '#default_value' => variable_get('custom_language_block_use_short_names', 0),
  );

  // TO DO needs validation
  $form['custom_language_block_separator'] = array(
    '#type' => 'textfield',
    '#title' => t('Separator'),
    '#description' => t('Enter separator value. If empty no separator will be used.'),
		'#default_value' =>  variable_get('custom_language_block_separator'),
  );
  return system_settings_form($form);
}